---
title: "Spike: How-to migrate to a monorepo"
---

{{% notice info %}} This page is probably not relevant for you as a FaSoz
developer. {{% /notice %}}

We considered moving from separate git repositories to a monorepo to be able to
share code more easily. After some experimentation (and given how far along the
project is at this point) it became apparent that there aren't enough benefits
to justify such a change.

It took some time to figure out how to merge separate repos into one and rewrite
the commit messages, so here is a rough guide in case someone wants to do
something similar in the future.

## How-to

- start with an empty directory `mkdir fasoz-migration && cd fasoz-migration`

### Clone repos you want to merge

- `git clone https://gitlab.com/fasoz/backend`
- `git clone https://gitlab.com/fasoz/frontend`
- `git clone https://gitlab.com/fasoz/mock-data-generator`

### Rewrite history

Before merging the repos into a monorepo, we're going to prefix all commit
messages with their repo name. This will result in cleaner looking commit
messages for the monorepo.

- `cd backend/`
- `git filter-branch --msg-filter 'printf "Backend: " && cat' -- --all`
- `cd ../frontend/`
- `git filter-branch --msg-filter 'printf "Frontend: " && cat' -- --all`
- `cd ../mock-data-generator/`
- `git filter-branch --msg-filter 'printf "Mock Data Generator: " && cat' -- --all`
- `cd ..`

### Initialize the monorepo

- `mkdir fallarchiv`
- `cd fallarchiv/`
- `git init`

### Merge the frontend repo into the monorepo

- `git remote add frontend ../frontend/`
- `git fetch frontend`
- `git merge frontend/main --allow-unrelated-histories `
- `mkdir frontend`
- Move all frontend files into the `frontend` subdirectory
- `git add .`
- `git commit -m "Moved frontend files to subdirectory"`

### Merge the backend repo into the monorepo

- `git remote add backend ../backend/`
- `git fetch backend`
- `git merge backend/main --allow-unrelated-histories`
- `mkdir backend`
- Move all backend files into the `backend` subdirectory
- `git add .`
- `git commit -m "Moved backend files to subdirectory"`

### Merge the mock-data-generator repo into the monorepo

- `git remote add mockdatagen ../mock-data-generator/`
- `git fetch mockdatagen`
- `git merge mockdatagen/main --allow-unrelated-histories`
- `mkdir mock-data-generator`
- Move all mock-data-generator files into the `mock-data-generator` subdirectory
- `git add .`
- `git commit -m "Moved mock-data-generator files to subdirectory"`

### Push the new monorepo to GitLab

- Create an uninitialized GitLab repo
- `git remote add origin https://gitlab.com/fasoz/fallarchiv`
- `git push --set-upstream origin main `

### Possible next steps

- combine the GitLab CI pipelines
- combine the `.idea` folders
