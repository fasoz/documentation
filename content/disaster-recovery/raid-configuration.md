---
title: "RAID configuration"
---

Redundancy is achieved using a two-disk
[Btrfs](https://en.wikipedia.org/wiki/Btrfs) volume configured as RAID1
(mirror).

## How the Btrfs RAID was installed and configured

Debian was installed on a single-disk Btrfs volume on `/dev/sda` with the root
mount pointing to `/dev/sda2`. The second disk `/dev/sdb` was partitioned in an
identical layout, however it was not used during the installation.

After the installation, the RAID was set up with the following commands:

```
btrfs device add -f /dev/sdb2 /
btrfs balance start -dconvert=raid1 -mconvert=raid1 /
```

Reference:
[Official Btrfs Wiki](https://btrfs.wiki.kernel.org/index.php/Using_Btrfs_with_Multiple_Devices#Conversion)

No further configuration (like editing fstab) is necessary! You can verify that
both disks are part of the same RAID1 volume with the commands:

```
btrfs filesystem show
btrfs filesystem df /
```

## Why the onboard RAID controller was not used

- There is no Linux support for it (CentOS might work, but we already decided on
  Debian)
- It's one of the cheap controllers that don't take any load off the CPU.

## Cloning the boot partition to the second drive

The server boots from EFI partition on the first disk (`/dev/sda1`). Should this
disk fail, we would not be able to boot from the second drive.

But since the second drive (`/dev/sdb`) was partitioned in an identical layout,
we can clone the boot partition with the command
`dd if=/dev/sda1 of=/dev/sdb1 status=progress`.

At the time of writing there is no automated process to keep the cloned
partition up to date.
