---
title: "Disaster Recovery"
weight: 3
---

This guide will walk you through the steps of getting everything up and running
after a worst case scenario desaster. I.e. everything, except source code and
documentation, is gone.

This guide also serves a double purpose as an introduction to our IT operations
and infrastructure.

## Prerequisites

A Linux machine is recommended. You need the following software:

- OpenSSH client
- Ansible
- Terraform
- kubectl

## Hardware

FASoz runs on a server located in the campus data center at the Frankfurt
University of Applied Sciences. The servers specifications (as of 2020-10-26)
are:

- Intel Xeon D-1518 4-Core 2,20GHz 6MB
- 32 GB ECC DDR4 2666 RAM
- 2x 480 GB SATA III Intel SSD 3D-NAND TLC running in RAID 1
- Gigabit LAN
- Out-of-band management via
  [IPMI](https://en.wikipedia.org/wiki/Intelligent_Platform_Management_Interface)

For the full specifications refer to the
[Thomas Krenn invoice located in our group folder](https://nextcloud.frankfurt-university.de/f/3366169).

Campus IT allotted two IP addresses for this server:

- [10.18.2.120](https://10.18.2.120/) for the out-of-band management web
  interface
- 10.18.2.121 - local IP used by the operating system
- 194.94.82.249 - public IP used by the operating system

## Passwords

See [Developer Onboarding -
Passwords]({{< ref "/developer-onboarding/#passwords" >}})

## Nameserver

The domains `fallarchiv.de` and `www.fallarchiv.de` must resolve to the public
IP of the server.

## Operating System

Currently we are running Debian 11. In the event that the operating system had
to be reinstalled, there are these options:

1. gain physical access to the server and plug in in a USB stick
2. mount an installation "floppy" from the
   [out-of-band management web interface](https://10.18.2.120/)
3. mount a DVD image from a Windows share via the
   [out-of-band management web interface](https://10.18.2.120/)

Option 2 is most likely no longer viable in the 21st century.

### Installation process

- Make sure to configure the correct static IP address.
- Install and enable an OpenSSH server.
- Set a root password.
- One may also want to follow the steps documented
  [here]({{< ref "/disaster-recovery/raid-configuration" >}} "RAID
  configuration"), on how to set up a BTRFS software RAID 1.

Do not customize the operating system any more than absolutely necessary. The
setup process is automated as much as possible (see
[Provisioning](#provisioning)).

## Provisioning

With this basic installation and an SSH server up and running, the server can be
provisioned using [Ansible](https://www.ansible.com/).

Provisioning is automated using the Ansible playbook found in the
[_provisioning_ repository](https://gitlab.com/fasoz/provisioning). This
documentation will not go into any details about the playbook and its roles and
tasks, because the playbook is effectively Documentation as Code. Reading its
source code is recommended.

### Running the Ansible playbook for the first time

{{% notice danger %}} **Beware!** Before running the playbook, make sure to
double check the
[`hosts.ini`](https://gitlab.com/fasoz/provisioning/-/blob/main/hosts.ini) file
and
[`network-configuration`](https://gitlab.com/fasoz/provisioning/-/tree/main/roles/network_configuration)
role and make changes if necessary. {{% /notice %}}

1. Make sure the server is reachable via SSH.
2. Generate a SSH key pair on your machine if you don't already have one  
   `ssh-keygen -a 128 -t ed25519`.
3. Copy your ssh public key to the FASoz server `ssh-copy-id root@10.18.2.121`.
4. Clone the provisioning repository
   `git clone https://gitlab.com/fasoz/provisioning`.
5. `cd provisioning/`.
6. Edit `roles/users/vars/main.yaml` and add your ssh public key.
7. Run the playbook `./play.sh`.

### kubeconfig

The Ansible playbook fetches the kubeconfig file that's created by k3s and
stores it in `secrets/kubeconfig`. This file is needed in order to access the
Kubernetes cluster in the next section.

## Kubernetes Deployments

Steps:

1. Clone the deployments repository
   `git clone https://gitlab.com/fasoz/deployments` to the same parent folder
   where the provisioning repository resides.
2. `cd deployments/terraform`
3. `terraform apply -var-file config.tfvars`
4. Find the `kubeconfig` file in the `secrets` subfolder.
5. Configure `kubectl` to use that `kubeconfig` file:
   `export KUBECONFIG=/path/to/secrets/kubeconfig`
6. create the GitLab registry secret as described
   [here]({{< ref "/operations-onboarding/deployments/kubernetes-secrets#gitlab-registry-secret" >}})
7. create the production secrets as described
   [here]({{< ref "/developer-onboarding/backend/production-secrets/" >}})

## Restore a backup

At the time of writing there are no automated backups (for lack of
infrastructure that meets our privacy requirements). So you need a
[manually created backup](https://gitlab.com/fasoz/deployments/-/blob/main/scripts/backup-fallarchiv.sh).

### Restore MongoDB databases

In order to be able to connect to MongoDB running in Kubernetes you need a
port-forwarding `kubectl -n mongodb port-forward service/mongodb 27017:27017`.`

If you don't have the MongoDB root password at hand, navigate to the
`deployments` folder and run `terraform output mongodb_root_password`.

#### Create prod user

1. Connect to MongoDB with mongo cli `mongo -u root fasoz-prod`. You will be
   asked for the MongoDB root password.
2. Set prod database: `use fasoz-prod`.
3. Create user:
   `db.createUser( { user: "fasoz-prod", pwd: "REDACTED", roles: ["readWrite"] })`.
4. `exit` to quit mongo cli.

Remember: the password must match with the one configured in the backend's
[`production.json`]({{< ref "/developer-onboarding/backend/production-secrets" >}}).

#### Import backup

1. Navigate to the files `cases` and `users` in your MongoDB backup.
2. Import "cases" collection `mongoimport -d fasoz-prod -u fasoz-prod cases`.
3. Import "users" collection `mongoimport -d fasoz-prod -u fasoz-prod users`.

### Restore Minio

1. Forward a local port to the Minio server running in Kubernetes
   `kubectl -n minio port-forward service/minio 9000:9000`
2. upload the contents of the `frafasoz-casefiles-prod` bucket
