---
title: "Operations Onboarding"
weight: 2
---

This section documents how to keep FaSoz running and maintained.

Please also check out the [disaster recovery]({{< ref "/disaster-recovery/" >}}
"disaster-recovery") section because things that are covered there are not
necessarily repeated in this section.

## Technologies

- [SSH](https://en.wikipedia.org/wiki/Secure_Shell)
- [Debian](https://www.debian.org/)
- [git](https://git-scm.com/)
- [VPN](https://confluence.frankfurt-university.de/display/SN1/Netz%3A+Knowledge+Base)
- [Ansible](https://www.ansible.com/)
- [Terraform](https://www.terraform.io/)
- [k3s Kubernetes](https://k3s.io/)
- [Podman](https://podman.io/) or [Docker](https://www.docker.com/)

## Passwords

We use as few passwords as possible. In cases where setting a password can't be
avoided it is stored in the
[KeePass database located in our Nextcloud group folder](https://nextcloud.frankfurt-university.de/f/4690200).
This database is protected by a master password. Contact your superiors if you
think you should have access.

## Recommended Editor

For the [provisioning](https://gitlab.com/fasoz/provisioning) and
[deployment](https://gitlab.com/fasoz/deployments) repositories any text editor
will do. [Visual Studio Code](https://code.visualstudio.com/) is not a bad
choice.

## Hardware and Location

We have one bare metal server located in the CIT data center, building 3,
room 10.  
As of 2020-11-25 it's only reachable on the intranet (i.e. VPN or campus WiFi).
The process of making it publicly available is a work in progress.

### Server Specifications

- Intel Xeon D-1518 4-Core 2,20GHz 6MB
- 32 GB (2x 16GB) ECC DDR4 2666 RAM 2 Rank ATP
- 2x 480 GB SATA III Intel SSD 3D-NAND TLC 2,5
- 2x 400 Watt redundantes Hot Swap PSU
- 6x 1Gbit LAN
- 2x 10Gbit LAN

## Software

The bare metal server runs a basic Debian 11 installation that is provisioned
with Ansible. For details please refer to the
[provionsing repository](https://gitlab.com/fasoz/provisioning).

## System and context diagram

![System and context diagram](/documentation/images/system-diagram.png "System and context diagram")

FASoz is largely a closed application with database, file storage, backend and
frontend running on the same host inside a single node [k3s](https://k3s.io/)
Kubernetes.  
For details on what services are reachable from the internet and intranet please
refer to the
["iptables" role of the Ansible "provisioning" playbook](https://gitlab.com/fasoz/provisioning/-/blob/main/roles/iptables/tasks/main.yaml).

It's main external dependency is the private GitLab container registry that
serves the Docker images that are built by our CI/CD pipelines. Access to the
registry is protected by a token (also see [Kubernetes
secrets]({{< ref "kubernetes-secrets.md" >}})).

Technically, the system updates downloaded from the Debian servers are also an
external dependency.
