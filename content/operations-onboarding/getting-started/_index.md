---
title: "Getting Started"
weight: 1
---

## Passwords

We try to avoid passwords wherever possible and use other security mechanism
instead. However there are about half a dozen passwords a FaSoz system
administrator needs. These passwords are stored in a _KeePass_ password store
which is protected by a master password. The password store is located in the
[FaSoz group folder](https://nextcloud.frankfurt-university.de/apps/files/?dir=/gf-frafasoz)
in the Fra-UAS Nextcloud.

If you don't have access to the group folder or don't have the KeePass master
password, please ask other project members for help.

If you are unsure which KeePass fork to use, we recommend
[KeePassXC](https://keepassxc.org/).

## Access to the local Fra-UAS network

The FaSoz server is only reachable via its local IPs `10.18.2.120` and
`10.18.2.121`. So in order to be able to access it you need to be connected the
Fra-UAS campus WiFi or the Fra-UAS VPN.

For more information on how to gain access to the Fra-UAS WiFi and VPN refer to
the
[Fra-Uas Knowledge Base](https://confluence.frankfurt-university.de/display/SN1/Netz%3A+Knowledge+Base).

## Out of Band Management (OoB) / Intelligent Platform Management Interface (IPMI)

It is possible to access the server remotely even if the operating systems
crashes or a critical software component (e.g. sshd) fails. The IPMI provides
access to various hardware sensors and settings, and most importantly, it allows
to access its monitor, mouse and keyboard as if you were sitting in front of the
physical machine.

The IMPI web UI can be access [here](https://10.18.2.120/). Remember that you
need to have access to the [Fra-UAS LAN]({{< ref "#passwords" >}}) to be able to
connect. Accessing the web UI also requires the admin password that is stored in
the [FaSoz KeePass password
store]({{< ref "#access-to-the-local-fra-uas-network" >}}).

## Root access to the server

First make sure that you have access to the Fra-UAS network and are able to
reach the SSH-Server at address `10.18.2.121`. There are two authentication
methods for accessing the server:

1. public key authentication
2. root password

### Public key authentication

The SSH keys of system administrators are provisioned by the
["users" Ansible role](https://gitlab.com/fasoz/provisioning/-/tree/main/roles/users).
Note that the provisioning using Ansible can only be performed by someone with
root privileges.

Add a user account with your ssh public key
[here](https://gitlab.com/fasoz/provisioning/-/edit/main/roles/users/vars/main.yaml).
Then ask one of the existing system administrators to run the Ansible playbook
to apply the updated user accounts to the server.

If you don't have the permission to submit changes to the provisioning repo,
contact one of the "owners" listed
[here](https://gitlab.com/groups/fasoz/-/group_members).

### Password authentication

You may simply connect via `ssh root@10.18.2.121` and enter the password store
in the FaSoz KeePass password store. It is strongly recommended to use public
key authentication. Password authentication should only be used as a fall back.

## Provisioning

"Server provisioning is a set of actions to prepare a server with appropriate
systems, data and software, and make it ready for network operation" (from
[Wikipedia](<https://en.wikipedia.org/wiki/Provisioning_(telecommunications)#Server_provisioning>)).
For the FaSoz server these actions are automated using
[Ansible](https://www.ansible.com/). Ansible uses so called "playbooks" which
may contain individual actions (called "tasks") and/or groups of actions (called
"roles"). Playbooks are effectively code and is therefore well suited to be
version controlled.  
The FaSoz playbook that provisions our bare metal server in the Fra-UAS data
center located in the
["provisioning" repository at GitLab](<https://en.wikipedia.org/wiki/Provisioning_(telecommunications)#Server_provisioning>).

The idea is that all changes in the operating system are performed using the
Ansible playbook. The playbook thereby precisely defines how the FaSoz servers
differs from a vanilla Debian 11 installation.

Ansible playbooks are written in a human-friendly YAML format, so on top of the
automation benefits, the playbook also serves as documentation for the OS and
software state.

If something were to go wrong with the FaSoz server, reprovisioning is as simple
as reinstalling Debian 11 and running the Ansible playbook.  
Note that the playbook's network configuration role is specific to the Fra-UAS
data center. So if the playbook is to provision another server (i.e. in case of
migrating to the cloud) make sure to modify the playbook accordingly.

{{<video autoplay="true" loop="true" src="ansible-provisioning.webm" >}}

How to run the Ansible playbook to provision the FaSoz server:

1. make sure you can reach the FaSoz server via ssh at IP `10.18.2.121`,
2. Clone the provisioning repository
   `git clone https://gitlab.com/fasoz/provisioning`,
3. `cd provisioning`,
4. run `./play.sh` to start Ansible.

Also see [Provisioning]({{< ref "/operations-onboarding/provisioning" >}}
"Provisioning").

## Kubernetes

The Ansible playbook described in the last section installs a k3s Kubernetes
single-node cluster on the FaSoz-Server. Kubernetes resources (like deployments,
ingresses, PVCs, etc.) are provisioned using Terraform, another infrastructure
as code software tool.

The Kubernetes resources provisioned using Terraform are defined in the
["deployments" Git repository](https://gitlab.com/fasoz/deployments). This is
effectively the target state. The actual state that Terraform observed during
runtime is stored [here](https://gitlab.com/fasoz/deployments/-/terraform).

For more information on how to use Terraform to make changes to the FaSoz
Kubernetes cluster, see
[Deployments]({{< ref "/operations-onboarding/deployments" >}} "Deployments").

## Kubernetes Deployments

See [Deployments]({{< ref "/operations-onboarding/deployments" >}}
"Deployments").
