---
title: "Provisioning"
weight: 2
---

[Provisioning](<https://en.wikipedia.org/wiki/Provisioning_(telecommunications)#Server_provisioning>)
is done using [Ansible](https://www.ansible.com/). Ansible is a tool for
defining software provisioning and configuration as code.  
[Each Ansible role in our provisioning repository](https://gitlab.com/fasoz/provisioning/-/tree/main/roles)
has a short Readme file explaining what it does. The YAML files in each role
also serve as documentation as code and are a recommended read if you want to
learn details on how the server is provisioned and configured.

{{% notice %}} **Note:** Kubernetes itself is provisioned using Ansible.
However, all resources inside the Kubernetes cluster (pods, services, etc) are
configured using the Terraform. For more information refer to the
[Deployments]({{< ref "operations-onboarding/deployments" >}} "deployments")
section.{{% /notice %}}

## Prerequisites

- [Ansible](https://www.ansible.com/) is installed.
- SSH access to our server.

## Get started

- Clone the provisioning repository
  `git clone https://gitlab.com/fasoz/provisioning`.
- `cd provisioning`.
- Run `./play.sh` to start Ansible.
