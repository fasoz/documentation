---
title: "Deployments"
weight: 3
---

We use [Terraform](https://www.terraform.io/) for configuring our Kubernetes
deployments with GitLab as the
[Terraform backend](https://www.terraform.io/docs/language/settings/backends/index.html).

## Prerequisites

- [Terraform](https://www.terraform.io/downloads.html) is installed
- Our Kubernetes cluster's `kubeconfig`

## Get started

1. Clone the deployments repository
   `git clone https://gitlab.com/fasoz/deployments` in the same parent directory
   as the "provisioning" repo.
2. `cd deployments/terraform/`
3. Have your
   [personal GitLab token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html)
   ready.
4. Initialize Terraform
   1. visit https://gitlab.com/fasoz/deployments/-/terraform
   2. find the "production" state
   3. select the "Actions" menu
   4. click "Copy Terraform init command"
   5. replace the GitLab token placeholder with your personal token
   6. run the commands
5. (optional) Verify the state as described below.

## Verifying the state

Changes made in the [deployments](https://gitlab.com/fasoz/deployments/)
repository are **not** automatically applied. Changes must be applied manually
from your computer.

{{% notice %}} **FYI:** It would be possible to have GitLab CI automatically
apply changes to a Kubernetes cluster. However this would require GitLab to have
access to our Kubernetes API. This is not possible because we don't want our
`kubeconfig` stored on GitLab and the Kubernetes API is only reachable from
within the Fra-UAS intranet.{{% /notice %}}

1. Make sure you're in the Fra-UAS intranet (use VPN for remote access).
2. Navigate to the `deployments/terraform` folder.
3. Run `terraform plan -var-file config.tfvars`.

Terraform will now compare the state to the configured resources. If they match
there will be a message like "No changes. Infrastructure is up-to-date.".
Otherwise you will be presented with a list of changes that have to be made in
order to bring the actual state inline with the configuration.

## Making changes

1. Make sure you're in the Fra-UAS intranet (use VPN for remote access).
2. Make the desired changes to the configured Terraform resources.
3. Apply the changes using `terraform apply -var-file config.tfvars`.
4. Commit and push your changes to the git repository.
