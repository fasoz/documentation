---
title: "Kubernetes secrets"
---

## Recommended reading

- [Official Kubernetes documentation for secrets](https://kubernetes.io/docs/concepts/configuration/secret/)

## List existing secrets

```bash
kubectl get secrets --all-namespaces # probably more verbose than you need
kubectl -n fallarchiv get secrets # FASoz production secrets
kubectl -n gitlab get secrets # GitLab Runner secret
```

## GitLab Registry Secret

The
[`fallarchiv.tf`](https://gitlab.com/fasoz/deployments/-/blob/main/terraform/FASoz.tf)
deployment pulls images from our private GitLab registry. A deploy token must be
configured manually once:

- [Create a deploy token](https://gitlab.com/groups/FASoz/-/settings/repository)
  with `read_registry` scope
- Run
  `kubectl -n fallarchiv create secret docker-registry regcred --docker-server=registry.gitlab.com --docker-username=<username> --docker-password=<password>`

## GitLab Runner Secret

The GitLab Runner expects the registration token to be stored in a Kubernetes
secret. You can find the registration token
[here](https://gitlab.com/groups/FASoz/-/settings/ci_cd).  
To create the Kubernetes secret run:  
`kubectl -n gitlab create secret generic gitlab-runner-secret --from-literal=runner-token="" --from-literal=runner-registration-token=123456789abcdef`.
