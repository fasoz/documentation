---
title: "Maintenance"
weight: 4
---

A guide for the typical maintenance tasks.

## Updating Debian packages

Our server uses
[unattended-upgrades](https://wiki.debian.org/UnattendedUpgrades) to
automatically install security and other updates. (Also see the corresponding
Ansible role
[here](https://gitlab.com/fasoz/provisioning/-/tree/main/roles/unattended_upgrades).)

However if you want to verify everything is up to date you have to:

- SSH into the machine.
- Run `sudo apt update` to update the package lists.
- Run `sudo apt upgrade` to update outdated packages.

## Updating the operating system

Once a new major Debian version is released, the previous version gets security
updates for about a year. So an update to the latest version must be performed
before support ceases.

The process of upgrading to a newer release is described in the
[Debian wiki](https://wiki.debian.org/DebianUpgrade).  
Typically after an upgrade everything works just as before, but of course there
is guarantee.

## Update k3s (Kubernetes)

You can find out about new k3s releases on
[k3s GitHub release page](https://github.com/k3s-io/k3s/releases).

1. Clone the provisioning repository
   `git clone https://gitlab.com/fasoz/provisioning/`
2. `cd provisioning`
3. Edit `roles/k3s/vars/main.yaml` and set the desired k3s version.
4. Make sure you can reach our server via SSH.
5. Run the `./play.sh` script to start Ansible.

## Update MongoDB / Minio S3 / Elasticsearch

1. First follow the steps described
   [here]({{< ref "operations-onboarding/deployments/#get-started" >}}
   "deployments").
2. Set the desired MongoDB Helm chart version in `terraform/mongodb.tf`.
3. Set the desired Minio S3 Helm chart version in `terraform/minio.tf`.
4. Set the desired Elasticsearch Helm chart version in
   `terraform/elasticsearch.tf`.
5. Run `terraform apply -var-file config.tfvars` inside the `terraform/` folder.

{{% notice info %}} These services are only reachable from within the Kubernetes
cluster. They are not exposed to the internet or intranet. Which arguably makes
regular and timely updates less important.{{% /notice %}}
