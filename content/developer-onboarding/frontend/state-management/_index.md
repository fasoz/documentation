---
title: "State Management"
---

Three different kinds of state management are used in this application.

1. [SWR](https://swr.vercel.app/) for fetching and mutating data from the
   [backend]({{< ref "/developer-onboarding/backend" >}} "backend").
2. [`useState` hook](https://reactjs.org/docs/hooks-state.html) when it
   suffices.
3. [`useContext` hook](https://reactjs.org/docs/context.html) for data about the
   currently logged in user.

Strictly speaking Formik is also managing state, but that's almost entirely
abstracted away.

## Why not Redux? Lessons learned from moving away from Redux

At first the plan was to have Redux manage all state and we have indeed
[used Redux for a while](https://gitlab.com/fasoz/frontend/-/merge_requests/3).
But it became clear
[after less than a month](https://gitlab.com/fasoz/frontend/-/commit/b8c3fcab06212ca3a0b6937f7d1771ebb3931743)
that Redux is overkill, because:

1. Forcing all state into Redux, just to have everything in one place and as a
   unified solution, is a lot of work that yields no real benefit.
2. For REST API calls, SWR requires a lot less boiler plate code while providing
   more functionality out of the box.
3. For simple things, a simple `useState` hook does just fine.
4. `useContext` is a good solution for having a global state that holds data
   about whether a user is logged in and who that user is. This is the only
   state in this application that's truly global.
5. Due to its complexity, Redux required more testing.

## Code examples

The following links show how the different kinds of statement are used in the
actual application

1. [How SWR is used to fetch, edit, update and delete cases](https://gitlab.com/fasoz/frontend/-/blob/65a5507cd45764e834d032c35fda4282ba2794d9/src/components/CaseContainer.tsx#L15).
2. [How `useState` is used to store search results](https://gitlab.com/fasoz/frontend/-/blob/65a5507cd45764e834d032c35fda4282ba2794d9/src/components/Search.tsx#L10).
3. The implementation for the `UserContext` can be found
   [here](https://gitlab.com/fasoz/frontend/-/blob/65a5507cd45764e834d032c35fda4282ba2794d9/src/UserContext.ts)
   and
   [here](https://gitlab.com/fasoz/frontend/-/blob/65a5507cd45764e834d032c35fda4282ba2794d9/src/App.tsx#L59).
   [Here](https://gitlab.com/fasoz/frontend/-/blob/65a5507cd45764e834d032c35fda4282ba2794d9/src/components/LoginForm.tsx#L83)
   is an example how `useContext` is used by the `LoginForm` component to
   determine whether a user is already logged.
