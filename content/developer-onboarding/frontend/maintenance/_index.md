---
title: "Maintenance"
---

A guide for the typical maintenance tasks that have to be performed for the
frontend.

## Update NPM packages

### Install updates

You can check for outdated packages by running `npm outdated`.  
`npm audit` checks for known vulnerabilities.

Packages can be updated automatically by executing `npm upgrade` and
`npm audit fix`.

Alternatively you can update individual packages to the latest version with
`npm install <package-name>@latest` or to a specific version (e.g. v1.2.3) with
`npm install <package-name>@1.2.3`

### Check if the applications still builds

Execute `npm start` and `npm run build`.

### Run the end-to-end tests

See [here]({{< ref "/developer-onboarding/frontend/testing/" >}} "testing").

### What if something breaks after an update

See
[here]({{< ref "developer-onboarding/backend/maintenance/#what-if-something-breaks-after-an-update" >}}
"maintenance").

## Update Node.js

The process is identical to the backend. See
[here]({{< ref "developer-onboarding/backend/maintenance/#update-nodejs" >}}
"maintenance").
