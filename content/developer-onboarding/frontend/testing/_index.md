---
title: "Testing"
---

The two types of testing done for the frontend are a simple smoke test and
end-to-end (E2E) testing using [Cypress](https://www.cypress.io/).

## How to run the end-to-end tests locally

This tutorial assumes that you have Docker or Podman installed and that you have
cloned both the
[frontend]({{< ref "/developer-onboarding/frontend#getting-started" >}}
"backend") and the
[backend]({{< ref "/developer-onboarding/backend#getting-started" >}} "backend")
repositories.

To be able run the end-to-end tests locally you first have start a temporary
Minio S3 server.

```bash
cd backend/scripts/
./start-temporary-minio-s3-server.sh
```

The backend has to be started in development mode. This exposes additional
[endpoints only used for end-to-end testing](https://gitlab.com/fasoz/backend/-/blob/main/src/routes/test.ts).

```bash
cd backend/
npm run dev
```

Start the frontend as usual

```bash
cd frontend/
npm start
```

Finally start Cypress

```bash
cd frontend/
npm run cypress:open
```

Select a test you would like to run or run all tests by clicking "run all specs"
in the top right.

## Smoke Test

The CI/CD pipeline automatically builds Docker images for tagged git commits
(e.g. tag `v1.2.3`). This image contains a NGINX webserver that serves a
production build of the FaSoz frontend. After that image is built and pushed to
the GitLab container registry, a container based on that image is started and a
HTTP GET request is made to the webserver running inside the container. If the
container fails to start or the HTTP request fails, the smoke test fails too.

The smoke test is defined in
[`.gitlab-ci.yml`](https://gitlab.com/fasoz/frontend/-/blob/main/.gitlab-ci.yml)
under the name `production_release_smoke_test`.

## Code examples

- [E2E tests for the login form](https://gitlab.com/fasoz/frontend/-/blob/65a5507cd45764e834d032c35fda4282ba2794d9/cypress/integration/login.spec.ts).
