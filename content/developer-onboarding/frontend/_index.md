---
title: "Frontend"
weight: 4
---

The frontend is a single page web application that communicates with the backend
via [REST](https://en.wikipedia.org/wiki/Representational_state_transfer).

## Quick start

Node Package Manager (NPM) is required to install dependencies and to start this
application. Once you have NPM installed you can start the application locally
with the following commands:

```bash
git clone https://gitlab.com/fasoz/frontend/
cd frontend
npm install
npm start
```

The application can start without a backend running, however it obviously wont
be able to fetch any data. See
[here]({{< ref "/developer-onboarding/backend#getting-started" >}} "backend") on
how to start the backend locally.

## Technology stack

The application uses

- [React](https://reactjs.org/)
- [Material UI](https://material-ui.com/) for styling
- [SWR](https://swr.vercel.app/) for REST API calls
- [Formik](https://formik.org/) for forms
- [YUP](https://github.com/jquense/yup) for input validation
- [Cypress](https://www.cypress.io/) for end to end testing
- and plenty of other libraries

For a complete list of used libraries refer to the application's
[`package.json`](https://gitlab.com/fasoz/frontend/-/blob/main/package.json).

## Repository structure

| Path                                                                            | Description                                                                           |
| ------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------- |
| [.idea/](https://gitlab.com/fasoz/frontend/-/tree/main/.idea)                   | [JetBrains WebStorm](https://www.jetbrains.com/webstorm/)'s project specific settings |
| [cypress/](https://gitlab.com/fasoz/frontend/-/tree/main/cypress)               | Location of the end-to-end tests. Adheres to the default Cypress folder structure.    |
| [public/](https://gitlab.com/fasoz/frontend/-/tree/main/public)                 | Static files used by React                                                            |
| [src/](https://gitlab.com/fasoz/frontend/-/tree/main/src)                       | Source Code                                                                           |
| [src/index.tsx](https://gitlab.com/fasoz/frontend/-/blob/main/src/index.tsx)    | Frontend's main entry point                                                           |
| [src/components/](https://gitlab.com/fasoz/frontend/-/tree/main/src/components) | React components (also see [component types](#component-types))                       |
| [src/services/](https://gitlab.com/fasoz/frontend/-/tree/main/src/services)     | Services provide an abstraction layer for all communication with the backend          |

### Component types

We distinguish between these types of components

- [Container components](https://reactpatterns.com/#container-component) fetch
  and manipulate data, do event handling, may do routing and typically hold some
  kind of state. They do not render data. You can identify container components
  by their names `*Container.tsx`.
- Presentational components (a.k.a dumb components, a.k.a. stateless UI
  components) just render data.
- Neither of the above. Following the
  [KISS principle](https://en.wikipedia.org/wiki/KISS_principle) we do not force
  all components into either of the above categories.

We split components into container and presentational components when it
provides benefits. But we keep it simple when it's sensible.

## CI pipeline

The frontend pipeline is defined
[here](https://gitlab.com/fasoz/frontend/-/blob/main/.gitlab-ci.yml).
