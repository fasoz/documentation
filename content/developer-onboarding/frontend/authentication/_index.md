---
title: "Authentication"
---

For authentication we store two types of tokens on the client side, one access
token and one refresh token. They are encoded and signed using
[JSON Web Tokens](https://jwt.io/).

- Access token. A short lived token that is passed by the client to the backend
  with every request via HTTP header.
- Refresh token. A long lived HTTP cookie that is used to request a new access
  token. Every time a refresh token is used it is replaced by a new one (token
  rotation).

The way we handle tokens is an established practice that protects the tokens
against [XSS](https://en.wikipedia.org/wiki/Cross-site_scripting) and
[CSRF](https://en.wikipedia.org/wiki/Cross-site_request_forgery) attacks.

## Recommended reading

There are plenty of learning resources, e.g.:

- [Introduction to JSON Web Tokens](https://jwt.io/introduction/).
- [The Ultimate Guide to handling JWTs on frontend clients (GraphQL)](https://hasura.io/blog/best-practices-of-using-jwt-with-graphql/).
  We don't use GraphQL but the principles still apply.
- [Refresh Tokens: When to Use Them and How They Interact with JWTs](https://auth0.com/blog/refresh-tokens-what-are-they-and-when-to-use-them/).

## Backend interactions

### Login

Valid login credentials:

<!-- prettier-ignore-start -->
{{<mermaid>}}
sequenceDiagram
    participant Frontend
    participant Backend
    participant Database
    Frontend->>Backend: POST /api/login { username: "alice@example.org", password: "highEntropyPassword" }
    Backend->>Database: Compare with the stored hashed password
    Database->>Backend: Hashed passwords match
    Note right of Backend: Backend generates access-token and refresh-token
    Backend->>Database: Store 'session' containing client information and refresh-token
    Backend->>Frontend: Set refresh-token as a HTTP-only cookie <br /> Return access-token with HTTP 200 response
    Note right of Frontend: Browser saves the refresh token
    Note right of Frontend: Frontend stores access-token in memory. Internal state is set to logged in.
{{< /mermaid >}}
<!-- prettier-ignore-end -->

Invalid login credentials:

<!-- prettier-ignore-start -->
{{<mermaid>}}
sequenceDiagram
    participant Frontend
    participant Backend
    participant Database
    Frontend->>Backend: POST /api/login { username: "alice@example.org", password: "admin123" }
    Backend->>Database: Compare with the stored hashed password
    Database->>Backend: Hashed passwords do not match
    Backend->>Frontend: 401 Unauthorized
{{< /mermaid >}}
<!-- prettier-ignore-end -->

### Token refresh

Assume the client has a valid refresh token, but the access token has expired or
is missing.

<!-- prettier-ignore-start -->
{{<mermaid>}}
sequenceDiagram
    participant Frontend
    participant Backend
    participant Database
    Frontend->>Backend: GET /api/auth/verify_session <br /> Browser automatically provides the refresh token via HTTP header
    Backend->>Database: Check if a 'session' with this refresh-token is stored
    Database->>Backend: session found
    Note right of Backend: Backend generates new access token and refresh token.
    Backend->>Database: Replace the consumed refresh-token associated with this 'session' with the new refresh-token
    Backend->>Frontend: Overwrite the consumed refresh-token cookie with the new refresh-token<br /> Return new access-token with HTTP 200 response
    Note right of Frontend: Frontend stores new access-token in memory.
{{< /mermaid >}}
<!-- prettier-ignore-end -->

### Logout

<!-- prettier-ignore-start -->
{{<mermaid>}}
sequenceDiagram
    participant Frontend
    participant Backend
    participant Database
    Note right of Frontend: Frontend deletes its access token
    Frontend->>Backend: GET /api/logout
    Backend->>Database: Delete 'session' with this refresh-token
    Database->>Backend: session found
    Backend->>Frontend: Delete HTTP cookie holding the refresh-token
    Note right of Frontend: Frontend sets its internal state to logged out
{{< /mermaid >}}
<!-- prettier-ignore-end -->

The JavaScript frontend is unable to delete this cookie itself, because the
refresh-token is stored as a HTTP-only cookie.

## Refresh logic

The frontend always tries to refresh the access-token on initial page load. Even
if there is no refresh-token.  
Since the refresh-token is stored as a HTTP-only cookie for security reasons,
its existence cannot be checked in JavaScript. So in order to determine whether
there is a refresh-token or not, the frontend tries get a new access-token by
accessing `/api/auth/verify_session`. Depending on the response (200, 204 or
401), the frontend can determine if a refresh-token exists and if the user is
logged in.

<!-- prettier-ignore-start -->
{{<mermaid>}}
graph TD
  pageLoad(START <br /> Initial page load)-->refreshAccessToken[Frontend attempts to refresh the access token. <br /> GET /api/auth/verify_session];
  refreshAccessToken-->backendResponse{Backend checks <br /> refresh-token <br /> and responds.};
  backendResponse-->|200|tokenRefreshed[Refresh-token does exist and is valid. <br /> Backend issued new access-token.];
  backendResponse-->|204|tokenMissing[Refresh-token is missing.];
  tokenMissing-->userIsNotLoggedIn(Frontend determines user is not logged in. <br /> END);
  backendResponse-->|401|tokenInvalid[Refresh-token is invalid <br /> because the refresh-token expired <br /> or the session was revoked.];
  tokenInvalid-->userIsNotLoggedIn;
  tokenRefreshed-->userIsLoggedIn[Frontend determines that user is logged in.];
  userIsLoggedIn-->storeNewToken[Frontend stores new access token.];
  storeNewToken-->waitForAccessTokenExpiry[Frontend starts a background <br /> task that waits until the <br /> access-token is almost expired.]
  waitForAccessTokenExpiry-->refreshAccessToken;
{{< /mermaid >}}
<!-- prettier-ignore-end -->

## Source code links

- [`LoginForm` component](https://gitlab.com/fasoz/frontend/-/blob/main/src/components/account/LoginForm.tsx)
- [`loginService`](https://gitlab.com/fasoz/frontend/-/blob/main/src/services/login.ts)
- [Logout button](https://gitlab.com/fasoz/frontend/-/blob/main/src/components/layout/TopNavigation.tsx#L129)
- [`refreshAccessToken` effect hook](https://gitlab.com/fasoz/frontend/-/blob/65a5507cd45764e834d032c35fda4282ba2794d9/src/App.tsx#L46)
