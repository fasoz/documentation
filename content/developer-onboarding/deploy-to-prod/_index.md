---
title: "Creating and deploying a new release"
weight: 2
---

## Manual steps

1. Increase the version number in `frontend/package.json`
2. `git commit` the changes
3. `git tag` the commit with a label, e.g. v1.2.3 (the leading 'v' is
   important!)
4. `git push`
5. The CI/CD pipeline now builds production release and pushes a new Docker
   image to our GitLab registry
6. Repeat steps 1 to 5 for `backend/package.json`
7. Wait for both CI/CD pipelines to finish
8. Update the version tags in `deployments/terraform/fallarchiv.tf`
9. Log into the university VPN
10. Run `terraform apply`
11. Check if Kubernetes has successfully deployed pods running the newest FASoz
    version `kubectl -n fallarchiv get pod`

## Partially automated steps

The following Bash script automates the version bumping, git tagging and updates
the Terraform template: https://gitlab.com/fasoz/deployments/-/snippets/2301877

1. Ensure that the repositories `frontend`, `backend` and `deployments` are in
   the correct location
2. Run the above script `./create-release.sh v1.2.3`
3. Double check that the script has done everything correctly
4. `git push` the changes of all three repositories
5. Wait for the frontend and backend CI/CD pipelines to finish
6. Log into the university VPN
7. Run `terraform apply`
8. Check if Kubernetes has successfully deployed pods running the newest FASoz
   version `kubectl -n fallarchiv get pod`
