---
title: "Getting Started"
weight: 1
---

This is a tutorial on how to set up a local development environment.

## Operating system prerequisites

This guide uses software and technologies from the _Linux_ ecosystem. However
most (if not all tools) used in this guide are also available for _Windows_ (via
_Bash on Windows_ or _WSL2_).

### Note for _Windows_ users

_Windows_ users should be familiar with the _WSL2_ (_Windows Subsystem for
Linux_) and particularly how to share folders between Windows and _WSL2_. _WSL2_
is recommended over _Bash for Windows_ because it allows to use the container
engine _podman_. It is possible to use _Bash for Windows_ in conjunction with
e.g. _Docker Desktop on Windows_, however this is not covered by this guide.
Recommended _Linux_ Distributions for _WSL2_ are _Debian 11_ or newer and the
latest stable _Fedora_.

### Note for _MacOS_ users

_MacOS_ users should know how to install common \*nix command line tools (e.g.
via _Homebrew_). Note that in order to get _podman_ working on _MacOS_ you need
to initialize and start a virtual machine via `podman machine init` and
`podman machine start`. _Docker Desktop on Mac_ is an alternative to _podman_,
but using _Docker_ is not covered by this guide.

This guide was not tested on MacOS.

## Installing IDE and software tools

First off install the tools used in this guide:
`sudo dnf install git npm podman` (Fedora),
`sudo apt update && sudo apt install git npm podman` (Debian, Ubuntu).

Next up an IDE or text editor is required.
[_JetBrains WebStorm_](https://www.jetbrains.com/webstorm/) is the recommended
IDE for this project. Both the [frontend](https://gitlab.com/fasoz/frontend) and
[backend](https://gitlab.com/fasoz/backend) repo come with an `.idea` folder
that pre-configures _WebStorm_ with _ESLint_ and _Prettier_. You can download a
[_WebStorm_ installer from the _JetBrains_ Website](https://www.jetbrains.com/webstorm/download/)
or you can install it via the
[_JetBrains Toolbox App_](https://www.jetbrains.com/toolbox-app/). _WebStorm_ is
commercial software and requires a license. Students and teachers may obtain a
free educational license [here](https://www.jetbrains.com/community/education/).

Of course feel free to user whatever IDE or text editor you prefer. But make
sure you configure it such that _Prettier_ and _ESLint_ are run automatically
either on save or prior to git commits.

{{% notice info %}}[_Prettier_](https://prettier.io/) is an opinionated code
formatter that ensures that all developers commit code following the same code
styles.{{% /notice %}}

{{% notice info %}}[_ESlint_](https://eslint.org/) performs static code analysis
and enforces _TypeScript_ rules to ensure code quality and strong(ish)ly typed
code.{{% /notice %}}

{{% notice danger %}}**IMPORTANT:** Be aware that _ESLint_ runs as part of our
CI/CD pipelines after every `git push`. Should you commit code that does not
pass ESLints automated inspection the pipeline will halt and is not possible to
deploy your changes to production.

This can be avoided by continuously having ESLint analyze your code as it is
written.{{% /notice %}}

## Obtaining the source code

We use the [_Git_](https://git-scm.com/) version control system for managing our
source code. The source code itself is hosted on GitLab.com in the
[FaSoz Group](https://gitlab.com/fasoz).

Everyone can download and modify the source code, but as a developer you most
likely want to upload your code changes as well. For this you have to be a
[member of the FaSoz GitLab Group](https://gitlab.com/groups/fasoz/-/group_members)
with the role developer, maintainer or owner. If you haven't had a role assigned
to you yet, contact any of the "owners" listed
[here](https://gitlab.com/groups/fasoz/-/group_members).

1. Create a parent folder for the FaSoz source code (e.g.
   `mkdir FaSoz-Code && cd FaSoz-Code`)
2. Clone the frontend repository / download the source code:
   `git clone https://gitlab.com/fasoz/frontend`
3. Clone the backend repository: `git clone https://gitlab.com/fasoz/backend`
4. Clone the mock-data-generator repository:
   `https://gitlab.com/fasoz/mock-data-generator`

Now there are three folders inside the `FaSoz-Code` folder named `frontend`,
`backend` and `mock-data-generator`. Each of these folders contains a _WebStorm_
project and its corresponding source code. Please note that each repository
needs to be opened in a separate WebStorm window in order to work with the
entire codebase at once.

## Installing JavaScript dependencies

When you open the repositories with _WebStorm_ for the first time, _WebStorm_
will detect the missing dependencies and display a message with the option to
install them. Either allow _WebStorm_ to install the dependencies or navigate to
the repository on the command line (e.g. `cd FaSoz-Code/frontend/`) and run the
command `npm install`. Both will install the dependencies via the _JavaScript_
package manager [_NPM_](https://www.npmjs.com/).  
Make sure the dependencies for all three repositories (frontend, backend and
mock-data-generator) are installed.

{{<video autoplay="true" loop="true" src="install-javascript-dependencies.webm" >}}

Side note if you're concerned about the 14 vulnerabilities shown for the
frontend in the screen recording above: all these warnings stem from
[react-scripts](https://www.npmjs.com/package/react-scripts) and they don't
affect the production build. You might be able to use these vulnerabilities to
run arbitrary code on your own computer, but that indirection is rather
pointless.

## Running local instances of the necessary infrastructure

On startup, the FaSoz backend tries to connect to _MongoDB_, _MinIO_ and
_Elasticsearch_. So in order to start the backend we first need to start these
services locally.

In the [backend repository](https://gitlab.com/fasoz/backend) you will find the
bash script `scripts/start-podman-pod-with-dev-infrastructure.sh `. This script
uses the _podman_ container engine to start container instances of the required
services. As [mentioned
previously]({{< relref "#install-ide-and-necessary-tools" >}}), _podman_ is
available for Linux, Windows and MacOS. If you are unable to install _podman_,
_Docker_ would be the obvious alternative. A simplified bash script for Docker
users is provided at
`scripts/start-docker-containers-with-dev-infrastructure.sh`.  
If you are unable to use neither _podman_ nor _Docker_, try installing and
starting the three applications directly in your operating system.

{{<video autoplay="true" loop="true" src="starting-dev-infrastructure.webm" >}}

Note that the data stored in these containers is not persistent and all data is
lost when stopping or restarting these containers. They are also started with
the `--rm` flag, which causes the container to be deleted after being stopped.

After running either of the above mentioned scripts, all three services are
accessible on `localhost` on their respective defaults ports. When starting the
backend in development mode, it expects all three services running on localhost
at their respective default ports. So no configuration of the backend is
necessary.

## Starting the backend

First make sure _MongoDB_, _MinIO_ and _Elasticsearch_ are running and reachable
via `localhost`. You can start the applications from the command line or by
setting up a run configuration in WebStorm.

### Setting up a _WebStorm_ "run configuration"

1. Open the frontend repository in _WebStorm_.
2. Click **ADD CONFIGURATION** in the top right of the WebStorm window. This
   opens a modal window.
3. In that modal window, click the **➕** symbol in top left to **Add New
   Configuration**.
4. Select **npm** from the drop down menu.
5. Make sure **Command** is set to **run**.
6. For **Scripts** select **dev** from the drop menu.
7. Click the **OK** button.
8. Click the green arrow / play button in the top right to start the backend in
   development mode.

{{<video autoplay="true" loop="true" src="setting-up-webstorm-run-configuration-and-starting-backend.webm" >}}

### Starting the backend from the command line

If you don't use _WebStorm_ you can start the backend from the command line as
follows:

```bash
$ cd FaSoz-Code/backend/
$ npm run dev
```

### Backend configuration and `NODE_ENV` modes

The backend can be started in three different modes: "development", "test" and
"production".

#### "development mode"

Development mode is activated by setting the environment variable
`NODE_ENV=development`. Starting the backend via `npm run dev` always activates
development mode, regardless whether it is started using WebStorm, on the
command line or otherwise.

In development mode, the configuration from the file `config/development.json`
is loaded, which expects _MongoDB_, _MinIO_ and _Elasticsearch_ to run on
`localhost`. This configuration matches up perfectly with the _Podman_ and
_Docker_ scripts mentioned in section ["Run local instances of the necessary
infrastructure"]({{< relref "#run-local-instances-of-the-necessary-infrastructure" >}}).

This mode also loads the
[test router](https://gitlab.com/fasoz/backend/-/blob/main/src/routes/test.ts)
which exposes dangerous REST API endpoints used for development and testing.
These endpoints allow for things like bootstrapping an admin account or wiping
the databases, without providing any kind of authentication or security
mechanism.

#### How does the "test mode" differ from "development mode"?

Test mode uses the configuration file `config/test.json` and is very similar to
development mode. It uses different names for the _MongoDB_ database, _MinIO_
bucket and _Elasticsearch_ index than development mode. This allows you to run
the test suite while being able to continue developing without test and
development mode interfering with each other.

The test mode configuration is also used in the test stage of the
[backend's GitLab CI pipeline](https://gitlab.com/fasoz/backend/-/pipelines).

#### How does the "production mode" differ from "development mode"?

Production mode loads the configuration from `config/production.json` which sets
parameters like service address corresponding to the production environment in
our self-hosted _k3s_ Kubernetes cluster on our bare metal machine running the
FraUAS data center.

Production does not load the aforementioned
[test router](https://gitlab.com/fasoz/backend/-/blob/main/src/routes/test.ts),
which exposes dangerous endpoints only meant for development and testing.

## Starting the frontend

The frontend can be started without the backend running. But this will of course
lead to timeouts when trying to access the backend's REST API.  
You can start the frontend from the command line, via _WebStorm_ or another
IDE/text editor of your choice.

### Setting up a _WebStorm_ "run configuration"

1. Open the backend repository in _WebStorm_.
2. Click **ADD CONFIGURATION** in the top right of the WebStorm window. This
   opens a modal window.
3. In that modal window, click the **➕** symbol in top left to **Add New
   Configuration**.
4. Select **npm** from the drop down menu.
5. Set **Command** to **start**.
6. **Scripts** should be grayed out and show **start**.
7. Click the **OK** button.
8. Click the green arrow / play button in the top right to start the backend in
   development mode.

{{<video autoplay="true" loop="true" src="setting-up-webstorm-run-configuration-and-starting-frontend.webm" >}}

### From the command line

If you don't use _WebStorm_ you can start the frontend from the command line as
follows:

```bash
$ cd FaSoz-Code/frontend/
$ npm start
```

## Filling the website with content

Now that you know how to run the FaSoz website locally, you probably want to
fill it with content. To create a user account you can register a new account
via the website and edit the database entry using Mongo Express to mark your
email address as verified and assign yourself roles (like administrator). But
there are also two way more convenient ways.

### Bootstrapping an admin account

The bash script `scripts/bootstrap-admin-account.sh` in the backend repository
creates a user account with a verified email address and the role
"administrator". It uses `curl` to send a POST request to
`/api/testing/create_admin`. This endpoint is only exposed in development and
testing environments.

{{<video autoplay="true" loop="true" src="bootstrap-admin-account.webm" >}}

### Generating random mock cases and users

The [mock-data-generator](https://gitlab.com/fasoz/mock-data-generator) allows
you to generate a bunch of user accounts and cases. Simply run it after the
back- and frontend are running and it will populate the website with content.

While the content is generated, the program will output which user accounts have
been created it and what their email addresses and passwords are. So instead of
having to bootstrap an admin account yourself, you may simply use one of the
generated accounts.

Note that rerunning the mock-data-generator will wipe the website and previously
generated content and all changes you have made are gone.

{{<video autoplay="true" loop="true" src="mock-data-generator.webm" >}}

## Making and submitting code modifications

Teaching you how to use _Git_ and _WebStorm_ is not in the scope of this
tutorial. Please familiarize yourself with these programs.

A typical development workflow may look like this:

1. Make the desired modifications.
2. Implement tests for your modifications.
3. Run the integration and E2E tests to ensure that your modifications haven't
   broken anything.
   1. If tests fail because you intentionally changed the applications behavior,
      update the tests to reflect the change in behavior.
   2. If tests fail because you unintentionally broke something, fix it.
4. Submit the modifications to GitLab by pressing **CTRL-K** in _WebStorm_ top
   open the Git commit screen.

## See Also

- [How to maintain the
  backend]({{< ref "/developer-onboarding/backend/maintenance" >}}
  "maintenance")
- [How to maintain the
  frontend]({{< ref "/developer-onboarding/frontend/maintenance" >}}
  "maintenance")
- [How to create and deploy a new
  release]({{< ref "/developer-onboarding/deploy-to-prod" >}} "How to create and
  deploy a new release")
