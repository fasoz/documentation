---
title: "Elasticsearch"
---

Elasticsearch powers the search functionality on our website. It requires a
search index to be maintained that holds a copy of the case data stored in
MongoDB.  
MongoDB is the single source of truth, while the Elasticsearch index should at
all times be an exact copy of that data. For this a synchronization method had
to put in place.

## How the synchronization works

Synchronization is done by the
[backend]({{< ref "/developer-onboarding/backend" >}} "backend"). All API
endpoints `/api/case*` that perform CRUD operations on the `case` collection in
MongoDB, also perform an equivalent operation on the Elasticsearch index.  
There is a small chance that the data in MongoDB and Elasticsearch index get out
of sync. So it will happen eventually!

## What to do if the index gets out of sync

The Elasticsearch index can be rebuild by sending a POST request to the
`/api/admin/rebuild_index` endpoint (part of the
[admin router](https://gitlab.com/fasoz/backend/-/blob/main/src/routes/admin.ts)).
The request requires a bearer token from a user with the `Administrator` role,
otherwise it fails with error 403.  
For ease of use the frontend has an
[Elasticsearch page](https://www.fallarchiv.de/admin/elasticsearch) only visible
to administrators that contains a button which triggers the above mentioned
request.

## Discussion of the quality / robustness of our synchronization solution

The synchronization is not very elegant. The
[Elasticsearch page](https://www.fallarchiv.de/admin/elasticsearch) in the
frontend makes this particularly clear as it's likely to confuse ordinary site
administrators who know nothing about the programmatic business logic in our
application.  
We chose this solution simply because it was quickest to implement. It has its
drawbacks, but at least it's simple. Hopefully it will be easy to understand and
maintain by future programmers joining / taking over the project.

### Why rebuilding the index must be triggered manually

We considered triggering the index rebuilt periodically (i.e. every hour or day)
inside the backend. But

- The backend is designed to be stateless and allow for multiple instances to
  run in parallel without interfering with each other. If multiple instances of
  the backend start the synchronization at the same time they would inevitably
  get in each others way. This could be solved by a locking mechanism, but
  that's extra complexity for a solution that's still not very elegant.
- Most of the time the index would be rebuild without that being necessary,
  creating unnecessary load on the backend and Elasticsearch. As the case
  archive grows, rebuilding the index will become increasingly computationally
  expensive.

### What an elegant solution might look like

Ideally the Elasticsearch index would be kept in sync automatically without
anyone ever noticing what's going on under the hood. One such solution could be
a standalone application separate from the backend that checks if MongoDB and
Elasticsearch are in sync and if not, perform the necessary actions to achieve
synchronization.  
There are applications for keeping MongoDB and Elasticsearch in sync, but they
were developed for different use cases and are not suitable for us.  
Writing our own sync application is definitely an option worth considering,
should time and budget constraints allow it.
