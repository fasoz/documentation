---
title: "Backend"
weight: 3
---

The monolithic backend provides a RESTful API for the frontend.

## Prerequisites

- Node Package Manager (NPM)
- Docker or Podman

## Quick start

You can start the application locally with the following commands:

```bash
git clone https://gitlab.com/fasoz/backend/
cd backend
npm install
scripts/start-podman-pod-with-dev-infrastructure.sh
npm run dev
```

It is important to have a MongoDB, Minio S3 server and Elasticsearch running
locally before starting the backend in a development environment!  
If you are using Docker instead of Podman you have to modify the contents of
`scripts/start-podman-pod-with-dev-infrastructure.sh`.

## Technology stack

The application uses

- [Express](https://expressjs.com/) web framework
- [Jest](https://jestjs.io/) and
  [SuperTest](https://github.com/visionmedia/supertest) for integration testing
- [Mongoose](https://mongoosejs.com/) for MongoDB object modelling
- [JSON Web Tokens](https://jwt.io/)
- [Minio S3 Client](https://docs.min.io/docs/javascript-client-api-reference)
- and plenty of other libraries, particularly Express middlewares

For a complete list of used libraries refer to the application's
[`package.json`](https://gitlab.com/fasoz/backend/-/blob/main/package.json).

## Repository structure

| Path                                                                             | Description                                                                                      |
| -------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------ |
| [.idea/](https://gitlab.com/fasoz/backend/-/tree/main/.idea)                     | [JetBrains WebStorm](https://www.jetbrains.com/webstorm/)'s project specific settings            |
| [config/](https://gitlab.com/fasoz/backend/-/tree/main/config)                   | Run-time configurations for different environments                                               |
| [scripts/](https://gitlab.com/fasoz/backend/-/tree/main/scripts)                 | (Bash) scripts that are useful during development or automate certain tasks                      |
| [src/](https://gitlab.com/fasoz/backend/-/tree/main/src)                         | Source code                                                                                      |
| [src/middlewares/](https://gitlab.com/fasoz/backend/-/tree/main/src/middlewares) | Custom [Express middlewares](http://expressjs.com/en/guide/using-middleware.html)                |
| [src/models/](https://gitlab.com/fasoz/backend/-/tree/main/src/models)           | [Mongoose models](https://mongoosejs.com/docs/models.html) that define our database schema       |
| [src/routes/](https://gitlab.com/fasoz/backend/-/tree/main/src/routes)           | [Express routers](https://expressjs.com/en/api.html#router) that provide API endpoints           |
| [src/services/](https://gitlab.com/fasoz/backend/-/tree/main/src/services)       | Abstraction layers for external services (like Minio S3)                                         |
| [src/utils/](https://gitlab.com/fasoz/backend/-/tree/main/src/utils)             | Utility functions. Their individual intents are described in their respective source code files. |
| [tests/](https://gitlab.com/fasoz/backend/-/tree/main/tests)                     | Test suite                                                                                       |
