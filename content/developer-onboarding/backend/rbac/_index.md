---
title: "Role-Based Access Control"
---

The FaSoz applications uses a typical role-based access control scheme that
assign roles to individual users, with each role having certain permissions tied
to them.

## User Roles

Users may have none, one or more of the following roles:

- Guest
- Researcher
- Editor
- Administrator

## Permissions

| Name                        | Description                                 | No role[^norole] | Guest | Researcher | Editor | Administrator |
| --------------------------- | ------------------------------------------- | :--------------: | :---: | :--------: | :----: | :-----------: |
| CasesListPublished          | List published cases                        |        ✅        |  ✅   |     ✅     |   ✅   |      ✅       |
| CasesListUnpublished        | List unpublished cases                      |        ❌        |  ❌   |     ❌     |   ✅   |      ✅       |
| CaseCreate                  | Create unpublished cases                    |        ❌        |  ❌   |     ✅     |   ✅   |      ✅       |
| CaseReadAnyPublished        | Read any published cases                    |        ✅        |  ✅   |     ✅     |   ✅   |      ✅       |
| CaseReadAnyUnpublished      | Read any unpublished cases                  |        ❌        |  ❌   |     ❌     |   ✅   |      ✅       |
| CaseReadOwnUnpublished      | Read own unpublished cases                  |        ❌        |  ❌   |     ✅     |   ✅   |      ✅       |
| CaseUpdateAny[^fileuploads] | Update any cases                            |        ❌        |  ❌   |     ❌     |   ✅   |      ✅       |
| CaseUpdateOwn[^fileuploads] | Update own cases                            |        ❌        |  ❌   |     ✅     |   ✅   |      ✅       |
| CaseDeleteAny               | Delete any cases                            |        ❌        |  ❌   |     ❌     |   ✅   |      ✅       |
| CaseDeleteOwn               | Delete own cases                            |        ❌        |  ❌   |     ✅     |   ✅   |      ✅       |
| CasePublishAny              | Publish any cases                           |        ❌        |  ❌   |     ❌     |   ✅   |      ✅       |
| CasePublishOwn              | Publish own cases                           |        ❌        |  ❌   |     ❌     |   ✅   |      ✅       |
| CaseUnpublishAny            | Unpublish any cases                         |        ❌        |  ❌   |     ❌     |   ✅   |      ✅       |
| CaseUnpublishOwn            | Unpublish own cases                         |        ❌        |  ❌   |     ✅     |   ✅   |      ✅       |
| UserList                    | List user accounts                          |        ❌        |  ❌   |     ❌     |   ❌   |      ✅       |
| UserCreate                  | Create user account[^createuser]            |        ✅        |  ✅   |     ✅     |   ✅   |      ✅       |
| UserReadBasic               | Read any user's basic information           |        ❌        |  ✅   |     ✅     |   ✅   |      ✅       |
| UserReadFull                | Read any user's full information            |        ❌        |  ❌   |     ❌     |   ✅   |      ✅       |
| UserUpdateAn                | Update any user account                     |        ❌        |  ❌   |     ❌     |   ❌   |      ✅       |
| UserUpdateSelf              | Update own user account                     |        ❌        |  ✅   |     ✅     |   ✅   |      ✅       |
| UserDeleteAny               | Delete any user account                     |        ❌        |  ❌   |     ❌     |   ❌   |      ✅       |
| UserDeleteSelf              | Delete own user account                     |        ❌        |  ✅   |     ✅     |   ✅   |      ✅       |
| UserRolesUpdate             | Assign/Unassign role(s) to any user account |        ❌        |  ❌   |     ❌     |   ❌   |      ✅       |

[^norole]:
    Users that are not logged in or users that have no roles associated with
    their account.

[^fileuploads]: Includes permission to upload and delete case files
[^createuser]: Everyone may create a new account.
