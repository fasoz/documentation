---
title: "Maintenance"
---

A guide for the typical maintenance tasks that have to be performed for the
backend.

## Update NPM packages

### Install updates

You can check for outdated packages by running `npm outdated`.  
`npm audit` checks for known vulnerabilities.

Packages can be updated automatically by executing `npm upgrade` and
`npm audit fix`.

Alternatively you can update individual packages to the latest version with
`npm install <package-name>@latest` or to a specific version (e.g. v1.2.3) with
`npm install <package-name>@1.2.3`

### Check if the applications still builds

After upgrading any of the dependencies, the first thing that should be done is
see if the application still builds and starts. Execute `npm run tsc` to run the
TypeScript compiler, then execute `npm start` to start the application.  
Remember that you need to have [MongoDB, Minio S3 and Elasticsearch running
locally]({{< ref "/developer-onboarding/backend#getting-started" >}}
"backend#getting-started") in order to start the application successfully.

### Run the test suite

See if all integration tests `npm run test` pass.

### What if something breaks after an update

Unfortunately things break all the time. Typically (and ideally) there is
something that can be done in the code base to fix the problem.  
In case of an upstream issue you can
[pin a specific version](https://docs.npmjs.com/about-semantic-versioning#using-semantic-versioning-to-specify-update-types-your-package-can-accept)
in the
[`package.json`](https://gitlab.com/fasoz/backend/-/blob/main/package.json)
until it is resolved.

## Update Node.js

### Update the Node.js version used in the CI pipeline

Update the `image` directive in
[`.gitlab-ci.yaml`](https://gitlab.com/fasoz/backend/-/blob/main/.gitlab-ci.yml).  
Verify that pipeline runs successfully and that all stages pass.

### Update the Node.js version used in production

The Node.js version that is used in production is determined by the base image
specified in the
[backend's Dockerfile](https://gitlab.com/fasoz/backend/-/blob/main/Dockerfile).

## Rebuild the Elasticsearch index

Should the search functionality on the website ever show odd behavior, like
incorrect results or no results at all, chances are the search index is out of
sync.

1. Login with your administrator credentials at https://www.fallarchiv.de
2. Click the red "Admin" button in the top right
3. select "Elasticsearch" from the dropdown menu
4. Click the big red "Suchindex synchronisieren" button in the middle.

{{% notice info %}} If you are a developer and feel so inclined, you are welcome
to implement some kind of automated recurring maintenance task that checks if
the index is out of sync and resynchronizes it if necessary. {{% /notice %}}

An out-of-sync index is unlikely to happen. But it can happen, so it will happen
as time converges towards infinity. 🤓
