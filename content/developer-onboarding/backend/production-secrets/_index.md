---
title: "Production secrets"
---

{{% notice danger %}} **Friendly reminder!** Never commit production secrets to
a git repository. Never bake production secrets into Docker images.
{{% /notice %}}

The production deployment of the FASoz backend expects a `default.json` file
containing configuration defaults and a `production.json` file containing
configuration specific to the prod environment, to be mounted into the backend
container.  
Configuration files contain sensitive information and are stored as a
[Kubernetes secret](https://kubernetes.io/docs/concepts/configuration/secret/).

You can use the
[`default.json`](https://gitlab.com/fasoz/backend/-/blob/main/config/default.json)
as is. However the
[`production.json`](https://gitlab.com/fasoz/backend/-/blob/main/config/production.json)
needs to be updated with the necessary passwords etc.  
Parameters explicitly set to `null` are typically those that you have to update
with the relevant data. Do not commit this file to git when it contains
sensitive data!

After you prepared both files you can create the Kubernetes secret by running
`kubectl -n fallarchiv create secret generic fallarchiv-prod-config --from-file=default.json --from-file=production.json`.
