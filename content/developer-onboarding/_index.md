---
title: "Developer Onboarding"
weight: 1
alwaysopen: true
---

The goal of this section is to introduce new developers to the essentials they
need to know to be able to develop for FASoz. This is done by

- giving pointers towards what technologies and software is important to know
- aiding code exploration using links to key sections of the source code

The goal is **not** to document sections of code or provide API specifications,
because sooner or later this kind of documentation will be outdated. The single
source of truth is the source code itself!

## Technologies

You need to be familiar with the following technologies to be able to develop
for FaSoz.

- [git](https://git-scm.com/)
- [NPM](https://www.npmjs.com/)
- [TypeScript](https://www.typescriptlang.org/)
- [ESLint](https://eslint.org/)
- [Podman](https://podman.io/) or [Docker](https://www.docker.com/)
- [GitLab CI](https://docs.gitlab.com/ee/ci/)

The [frontend]({{< ref "/developer-onboarding/frontend" >}} "frontend") and
[backend]({{< ref "/developer-onboarding/backend" >}} "backend") each have
additional requirements.

## Language

FASoz is a German website. However this documentation, the source code and
pretty much everything "under the hood" is in English. This is to:

- keep the possibility open for having international developers
- avoid awkward mixed language variable names, function names, etc
- spare the reader from confusing translations of well established technical
  English terms

The rules for what language to use when are simple:

- if it's a string that's displayed to an end-user on the website (i.e. the
  frontend) ⇨ use German
- else ⇨ use English

## Code Style

The frontend and backend are written in
[TypeScript](https://www.typescriptlang.org/) and use
[ESLint](https://eslint.org/) for code linting and
[Prettier](https://prettier.io/) for unified code styling and formatting. Our
code styles are documented as code in their respective configurations (see table
below).

|                          | Frontend                                                                           | Backend                                                                           |
| ------------------------ | ---------------------------------------------------------------------------------- | --------------------------------------------------------------------------------- |
| TypeScript configuration | [tsconfig.json](https://gitlab.com/fasoz/frontend/-/blob/main/tsconfig.json)       | [tsconfig.json ](https://gitlab.com/fasoz/backend/-/blob/main/tsconfig.json)      |
| ESLint configuration     | [.eslintrc](https://gitlab.com/fasoz/frontend/-/blob/main/.eslintrc)               | [.eslintrc](https://gitlab.com/fasoz/backend/-/blob/main/.eslintrc)               |
| Prettier                 | [.prettierrc.json](https://gitlab.com/fasoz/frontend/-/blob/main/.prettierrc.json) | [.prettierrc.json](https://gitlab.com/fasoz/backend/-/blob/main/.prettierrc.json) |
| EditorConfig             | [.editorconfig](https://gitlab.com/fasoz/frontend/-/blob/main/.editorconfig)       | [.editorconfig](https://gitlab.com/fasoz/backend/-/blob/main/.editorconfig)       |

## Recommended IDE

The recommended IDE for the [frontend](https://gitlab.com/fasoz/frontend) and
[backend](https://gitlab.com/fasoz/backend) repositories is
[JetBrains WebStorm](https://www.jetbrains.com/de-de/webstorm/) (free
[educational licenses](https://www.jetbrains.com/community/education/) are
available). These repositories have an `.idea` folder that holds the WebStorm
settings, so opening the repository with WebStorm will have linting and
formatting already configured.

<!-- prettier-ignore-start -->
{{% alert theme="warning" %}}**Please beware!** Linting is enforced by the CI pipeline and committing code that throws linting or TypeScript errors will cause the pipeline to fail.{{% /alert %}}
<!-- prettier-ignore-end -->
