---
title: "Documentation"
weight: 5
---

This documentation is written in
[Markdown](https://en.wikipedia.org/wiki/Markdown) that is compiled into a
static website by [Hugo](https://gohugo.io/). The theme we use is
[DocPort](https://docport.netlify.app/).

## Changing the documentation

To make changes to the documentation simply clone the git repository
`git clone https://gitlab.com/fasoz/documentation`, make your changes and commit
them.

## Local preview

First you must have [Hugo](https://gohugo.io/) installed on your machine. You
may then run `hugo server` in the documentation folder. This starts a local
webserver that automatically compiles the Markdown files to HTML.  
Hugo also detects changes and updates the preview in near realtime.

## How to publish changes to the documentation to [fasoz.gitlab.io](https://fasoz.gitlab.io/documentation/)

Changes pushed to the [main](https://gitlab.com/fasoz/documentation/-/tree/main)
branch trigger the
[GitLab CI pipeline](https://gitlab.com/fasoz/documentation/-/blob/main/.gitlab-ci.yml)
that rebuilds the website and publishes it.
