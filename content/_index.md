---
title: "FaSoz Technical Guide"
hide:
  - nextpage
---

Welcome to the technical documentation of the "Fallarchiv Soziale Arbeit"
(FaSoz) project. This documentation is primarily aimed at developers and
administrators.

The FaSoz website itself is hosted at
[www.fallarchiv.de](https://www.fallarchiv.de/).

## About this documentation

This documentations has three main sections. If you want to develop (i.e. write
code) for FaSoz refer to the [developer
onboarding]({{< ref "/developer-onboarding/" >}} "developer-onboarding")
section. If your task is to run and maintain the FaSoz server and its
applications check out the [operations
onboarding]({{< ref "/operations-onboarding/" >}} "operations-onboarding") and
[disaster recovery]({{< ref "/disaster-recovery/" >}} "disaster-recovery")
sections.

## Git repositories

All our repositories and CI pipelines are hosted on GitLab. A list of
repositories and short descriptions can be found in our GitLab group at
https://gitlab.com/fasoz.

It is recommended to clone all git repositories into the same directory because
some of the infrastructure/deployment related scripts access files from both
repositories.

## Technologies

- Bare metal server located on campus
- Debian 11
- Single node Kubernetes
- MongoDB document database
- Minio S3 file storage
- REST API backend
- Single page web application

This is just a high-level overview. Each subsection goes into greater detail on
what technologies are used.
