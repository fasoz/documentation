# FASoz Technical Guide

A static site generated from markdown files using [Hugo](https://gohugo.io/).  
This site is hosted on GitLab Pages at https://fasoz.gitlab.io/documentation/.

## Start a local Hugo server

First [install Hugo](https://gohugo.io/getting-started/installing/), then run:

```bash
git clone https://gitlab.com/fasoz/documentation
cd documentation
hugo server -D
```
